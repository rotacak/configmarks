# Config Marks 1.0

It is intended for complicated config properties and their relationships to each other.

![preview](https://bytebucket.org/rotacak/configmarks/raw/d74a4380aea0f2deb332e7adc15dcbc928f9260d/explanation.png)

See full explanation.htm